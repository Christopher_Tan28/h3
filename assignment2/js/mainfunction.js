"use strict"
//This is mainfunction.js

// create country datalist
let listCountry=document.getElementById("countryOptions")

for(let i=0;i<countryData.length;i++){
  let option=document.createElement('option')
  option.value=countryData[i]
  listCountry.appendChild(option)
}

let link="https://eng1003.monash/OpenFlights/allroutes/" // URL for allRoutes 

// THis global variable is used to store the sourceAirport ID.
let routeInfo=[]
//THis global variable is to store the destination airport id
let destinationInfo=[]
// this variable is to store the info of aiports API. 
let airports=[]
// this variable is to store the airport id. 
let airportIdd=[]

// get the element by id of the chosen country.
let place=document.getElementById("chooseCountry")
let displayName=document.getElementById("tripName")
let displayDate=document.getElementById("date")

// new calss object
let myTrip=new Trip()

// THis is to store the IATA-FAA info of the airport.
let myAirports=[]

// This variable is to store the property of map source ID.
let sourceList=[]


mapboxgl.accessToken='pk.eyJ1IjoiaGVucnkwMDIyNyIsImEiOiJja25zcWIzMXUwODMzMnR0N25qaXd6bm5iIn0.4TVLwESMHlleInUTLXFIag'

let caulfield = [145.0420733, -37.8770097];

let map = new mapboxgl.Map({
                    container: 'map',
                    style: 'mapbox://styles/mapbox/streets-v10',
                    zoom: 2,
                    center: caulfield
                });



/*This function is used to show the country in the map
when the country has been chosen. */
function mapInfo()
{    
    console.log(place.value)
    localStorage.setItem("place",place.value)
    let queryStirng="https://eng1003.monash/OpenFlights/airports/?country="+place.value+"&callback=displayAirports"
    console.log("text")
    let script1=document.createElement("script")
     script1.src=queryStirng
     document.body.appendChild(script1)
}






// THis is the callback function of mapInfo 
function displayAirports(inputs)
{
    for(let i=0;i<inputs.length;i++)
    {
        airports.push(inputs[i])
        airportIdd.push(inputs[i].airportId)
    }
    let key={
        country: place.value,
        callback:'getAirport'
    }
    webServiceRequest(link,key)
}


//Callback function of all routes. 
//THis function is used to make some restrictions so that it only show the airports with flights. 
function getAirport(results)
{
    let getlocation=[] 
    console.log(results)
    for(let i=0;i<results.length;i++)
    {
        getlocation.push({sourceAirport:results[i].sourceAirportId,destinationAirport:results[i].destinationAirportId})
    }
     let newPlace=[airports[0].longitude,airports[0].latitude]
     console.log(newPlace)
     map.panTo(newPlace)
    console.log(getlocation)
    // This nexted for loop is to make sure the map only show the airports with the same Id. 
    for(let i=0;i<getlocation.length;i++)
    {
        for(let j=0;j<airports.length;j++)
        {
            if(getlocation[i].sourceAirport==airports[j].airportId)
            {
                  let marker = new mapboxgl.Marker(
                  ).setLngLat([airports[j].longitude,airports[j].latitude])
                  .setPopup(new mapboxgl.Popup().setHTML(airports[j].name))
                  .addTo(map);
                // eventlistener for each marker to pass the getlocation in that marker.
                marker.getElement().addEventListener('click',function(){
                    showRoute(airports[j]["IATA-FAA"])
                }) 
            }
        }
        
        
    }
}





//This function is used to grab the info of the routes. 
function showRoute(result)
{
    if(!myAirports.includes(result))
    {
        myAirports.push(result) 
            for(let i=0;i<airports.length;i++)
            {
                if(result==airports[i]["IATA-FAA"])
                {
                myTrip.addAirport(airports[i])
                 if(!routeInfo.includes(airports[i].airportId))
                 {
                     routeInfo.push(Number(airports[i].airportId))
                 }

                console.log("textttt")
                }
            }
    }
    console.log(result)
    let placeRoute=localStorage.getItem("place")
    let keyData={
        country: placeRoute,
        callback: "displayMapInfo"
    }
    console.log(result)
    webServiceRequest(link,keyData)

}




//This fuction is used to display route
function displayMapInfo(allRoutes)
{
    //This is used to remove layer and change the color for the chosen route. 
    if(sourceList !=[])
    {
        for( let i=0;i<sourceList.length;i++)
        {
          if(!myAirports.includes(sourceList[i]))
          {
              map.removeLayer(sourceList[i])
              map.removeSource(sourceList[i])
          }
          else
          {
              map.setPaintProperty(sourceList[i], "line-color","#a81321")
          }
        }
        
        
    }
    //get rid of the situation that the source still has the sourceID. SInce each route must a unique Id.
    sourceList=[]
    //console.log(sourceList)
    for(let i=1;i<myAirports.length;i++)
    {
        sourceList.push(myAirports[i])
    }
    destinationInfo=[]
    for(let i=0;i<allRoutes.length;i++)
    {
        //This aim of this if statement is to check whether the destination is valid or not. 
        if(allRoutes[i].sourceAirportId==routeInfo[routeInfo.length-1] & !destinationInfo.includes(allRoutes[i].destinationAirportId& airportIdd.includes(allRoutes[i].destinationAirportId)))
        {
            destinationInfo.push(allRoutes[i].destinationAirportId)
            console.log("text")
        }
        
    }
        for(let i=0;i<destinationInfo.length;i++)
        {
            for(let j=0;j<airports.length;j++){               
                if(destinationInfo[i]==airports[j].airportId & !routeInfo.includes(destinationInfo[i]) & !sourceList.includes(airports[j]["IATA-FAA"]))
                {
                    sourceList.push(airports[j]["IATA-FAA"])
                    map.addSource(airports[j]["IATA-FAA"], {
                        'type': 'geojson',
                        'data': {
                        'type': 'Feature',
                        'properties': {},
                        'geometry': {
                            'type': 'LineString',
                            'coordinates': [
                                [myTrip.destination.longitude, myTrip.destination.latitude],
                                [airports[j].longitude,airports[j].latitude],
                    
                                 ]
                                }
                            }
                    });
                    map.addLayer({
                        'id': airports[j]["IATA-FAA"],
                        'source':airports[j]["IATA-FAA"],
                        'type': 'line',
                        'layer':{
                            'type':"LineString"
                        },
                        'layout': {
                            'line-join': 'round',
                            'line-cap': 'round'
                        },
                        'paint': {
                            'line-color': 'Black',
                            'line-width': 3
                        }
                    });
                } 
            }
        }
    console.log(destinationInfo)
}


/*undo function used to undo the last added destination
run when user click undo button */
function undoInfo()
{
    // use pop method to undo the last chosen destination.
    if(myTrip.route.length>1)
    {
        myTrip.route.pop()
        myAirports.pop()
        routeInfo.pop()
       
        showRoute(myAirports[myAirports.length-1])
    }
    else
    {
       for( let i=0;i<sourceList.length;i++)
       {
              map.removeLayer(sourceList[i])
              map.removeSource(sourceList[i])
          }
        //redefine to make sure the ID will not repeat
        sourceList=[]
        myTrip.route=[]
        myAirports=[]
        routeInfo=[]
        mapInfo()
    }
    
}




// This function is used to comfirm what has been chosen
function confirmInfo()
{
    if(myTrip.route.length===0)
    {
        alert("you have not choosen any airports on the map, please choose some")
    }
    else if(myTrip.route.length===1)
    {
        alert("you just choose a starting airport, please choose 1 or more destinations")
    }
    else
    {
        myTrip.name=displayName.value
        myTrip.date=displayDate.value
        myTrip.startPoint=myTrip.route[0].city
        myTrip.stops=myTrip.route.length-1
        let routes=[]
        //This part is calculating distance
        for(let i=0;i<myTrip.route.length;i++)
        {
            routes.push([myTrip.route[i].longitude,myTrip.route[i].latitude])
        }
        let totalDistance=turf.lineString(routes)
        let estimateDistance
        estimateDistance=Math.round(turf.length(totalDistance,{units:'kilometers'}))
        myTrip.lengthOfTrip=estimateDistance
        // show the info
        let notes=''
        notes+='Country: ' + place.value+ "\n"
        notes+='Start Date: '+displayDate.value +'\n'
        notes+='Start City: '+myTrip.route[0].city +' via '
        for(let i=1;i<myTrip.route.length;i++){
            notes+=myTrip.route[i].city+', '
        }
        notes+='\n'+'Stops: ' + myTrip.stops
        notes+='\n'+ 'Total distance: '+myTrip.lengthOfTrip+'Km'
        //give user an option
        let confirmation=confirm(notes)
        if(confirmation)
        {
            console.log(myTrip)
            trips.addTrip(myTrip)
            updateLocalStorage(trips)
            console.log(trips)
            let indexOfTrip=trips.count-1
            localStorage.setItem("TRIP_INDEX_KEY",indexOfTrip)
        }
    }
    
}




// This function is used to save the info of the map
function saveInfo()
{
    let confirming=confirm('are you sure you want to save this info?')
    if(confirming)
    {
       let flightTime=new Date(myTrip.date)
       let today=new Date()
       window.location="viewPage.html"
    }
    else
    {
       window.location="mainpage.html"
    }
}

// This is service.js in summary, copy that into this place.
function webServiceRequest(url,data)
{
	// Build URL parameters from data object.
    let params = "";
    // For each key in data object...
    for (let key in data)
    {
        if (data.hasOwnProperty(key))
        {
            if (params.length == 0)
            {
                // First parameter starts with '?'
                params += "?";
            }
            else
            {
                // Subsequent parameter separated by '&'
                params += "&";
            }

            let encodedKey = encodeURIComponent(key);
            let encodedValue = encodeURIComponent(data[key]);

            params += encodedKey + "=" + encodedValue;
         }
    }
    let script = document.createElement('script');
    script.src = url + params;
    document.body.appendChild(script);
}

























