/** 
 * shared.js 
 * This file contains shared code that runs on all different page.
 */

"use strict";
// Constants used as KEYS for LocalStorage
const TRIP_INDEX_KEY = "selectedTripIndex";
const TRIP_DATA_KEY = "tripLocalData";

// TODO: Write code to implement the Trip class
class Trip{
     constructor(){
        this._name=""
        this._date=""
        this._lengthOfTrip=0
        this._startPoint=""
        this._stops=""
        this._route=[]
        this._createdDate=new Date()
    }
    get route(){
        return this._route
    }
    get name(){
        return this._name
    }
    get date(){
        return this._date
    }
    get lengthOfTrip(){
        return this._lengthOfTrip
    }
    get startPoint(){
        return this._startPoint
    }
    get destination(){
        return this._route[this._route.length-1]
    }
    get stops(){
        return this._stops
    }
    set route(newOne){
        this._date=newOne
    } 
    set name(newId){
        this._name=newId
    }
    set date(newDate){
        this._date=newDate
    }
    set lengthOfTrip(newLength){
        this._lengthOfTrip=newLength
    }
    set startPoint(text){
        this._startPoint=text
    }
    set stops(newStop){
        this._stops=newStop
    }
    addAirport(airport){
        this._route.push(airport)
    }
  
    fromData(data){
        this._route=data._route
        this._name=data._name
        //this date is the start Date
        this._date=new Date(data._date)
        this._lengthOfTrip=data._lengthOfTrip
        this._startPoint=data._startPoint
        this._stops=data._stops
        // createdDate is the time the trip is made
        this._createdDate=new Date(data._createdDate)
    }
}
 
// TODO: Write code to implement the account class

class account{
    constructor(){
        this._trips=[]
        //this._user=""
        //this._password=""
    }
    get trips(){return this._trips}
    get count(){return this._trips.length}
    addTrip(trip){
        this._trips.push(trip)
    }
    getTrip(index){
        return this._trips[index]
    }
    
    //for remove trip, we need to find the index first, Then we can use splice.
    removeTrip(index){
      this._trips.splice(index,1)
    }
    /* since the Trips is a array consisting of many objects, so we need to use
    for loop to interate each element in the array, then push back into new object. */
    fromData(data){
        let newTrip=data._trips
        this._trips=[]
        for(let i=0;i<newTrip.length;i++){
            let trip=new Trip()
            trip.fromData(newTrip[i])
            this._trips.push(trip)
        }
    }
}
// TODO: Write the function checkIfDataExistsLocalStorage

//This function is simply used for check the data if it is in localStorage.
function checkIfDataExistsLocalStorage(){ 
  let data=getDataLocalStorage()
   
  //if(data) it will ckeck all the conditions automatically
  if(data)
  {
    return true
  }
  else
  {
    return false
  }
}

// TODO: Write the function updateLocalStorage

//put data in localstorage
function updateLocalStorage(data){
    let strdata=JSON.stringify(data)
    localStorage.setItem("TRIP_DATA_KEY",strdata)
}

// TODO: Write the function getDataLocalStorage

//get data from the localstorage
function getDataLocalStorage()
{
    let newData=JSON.parse(localStorage.getItem("TRIP_DATA_KEY"))
    return newData
}


// Global account instance variable
let trips = new account();
// TODO: Write the code that will run on load here

/* This is to check whether the localStorage has some data or not
 if not, updata the data. */

let mycheck=checkIfDataExistsLocalStorage()

if(mycheck===true)
{
    
    let data=getDataLocalStorage()

    trips.fromData(data)
} /*else{
    trips.addTrip("BOOM")
    updateLocalStorage(trips)
}*/
