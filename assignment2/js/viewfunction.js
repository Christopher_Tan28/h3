"use strict"
// There are some functions in the view page.

//map 
mapboxgl.accessToken='pk.eyJ1IjoiaGVucnkwMDIyNyIsImEiOiJja25zcWIzMXUwODMzMnR0N25qaXd6bm5iIn0.4TVLwESMHlleInUTLXFIag'
let caulfield = [145.0420733, -37.8770097];

let map = new mapboxgl.Map({
                    container: 'newMap',
                    style: 'mapbox://styles/mapbox/streets-v10',
                    zoom: 4,
                    center: caulfield
                });


// This function is to display all the info of the trips
function summaryDisplay()
{
   let nameDisp=document.getElementById('nameOfTrip') 
   let dateDisp=document.getElementById('dateOfTrip')
   let InitialPlace=document.getElementById('startPlace')
   let routeDisp=document.getElementById('destinationInfo')
   nameDisp.innerText=chosenTrip.name
   dateDisp.innerText=chosenTrip.date.toDateString()
   InitialPlace.innerText=chosenTrip.route[0].country
   let stringOfRoute=''
   stringOfRoute='Start Airport: '+chosenTrip.route[0].city
   if(chosenTrip.route.length<=2)
    {
        stringOfRoute+='\n'+'Destination: '+chosenTrip.destination.city
        stringOfRoute+='\n'+'Stops: '+chosenTrip.stops
        stringOfRoute+='\n'+'Distance: '+chosenTrip.lengthOfTrip+'km'
       
    }
    else
    {
   for(let i=1;i<chosenTrip.route.length-1;i++)
    {
        stringOfRoute+='\n'+'via '+chosenTrip.route[i].city
    }
    stringOfRoute+='\n'+'Destination: '+chosenTrip.destination.city
    stringOfRoute+='\n'+'Stops: '+chosenTrip.stops
    stringOfRoute+='\n'+'Distance: '+chosenTrip.lengthOfTrip+'km'
   }

   routeDisp.innerText=stringOfRoute
  
   //Display MapInfo
   let airportLocation=[chosenTrip.route[0].longitude,chosenTrip.route[0].latitude]
   map.panTo(airportLocation)
   for(let i=0;i<chosenTrip.route.length;i++)
   {
       let marker = new mapboxgl.Marker(
                  ).setLngLat([chosenTrip.route[i].longitude,chosenTrip.route[i].latitude])
                  .setPopup(new mapboxgl.Popup().setHTML(chosenTrip.route[i].name))
                  .addTo(map);
   }
          
}

function textStyle()
{
   let routeLocation=[]
   for(let i=0;i<chosenTrip.route.length;i++)
   {
       routeLocation.push([chosenTrip.route[i].longitude,chosenTrip.route[i].latitude])
   }
    
     map.addSource('myRoute', {
                'type': 'geojson',
                'data': {
                'type': 'Feature',
                'properties': {},
                'geometry': {
                 'type': 'LineString',
                'coordinates': 
                     routeLocation
                    
                    }
                    }
                });
            map.addLayer({
                'id': 'monash',
                'source':'myRoute',
                'type': 'line',
                'layer':{
                    'type':"LineString"
                },
                'layout': {
                'line-join':'round',
                'line-cap': 'round'
                },
                'paint': {
                'line-color': '#a81321',
                'line-width': 6
                }
                });
   
}

/*this function will run when click the cancel button
and it will delete the current trip in account(triplist)*/
function deleteView()
{
    let todayTime=new Date().getTime()
    if(chosenTrip.date.getTime()>todayTime)
    {
        let confirmation=confirm('Are you sure you want to cancel this trip?')
        if(confirmation)
        {
            trips.removeTrip(tripIndex)
            updateLocalStorage(trips)
            window.location="futurepage.html"
        }
    }
    else
    {
    alert('you cannot cancel this trip since it is the past trip')
    }
}

// Retrieve the stored index from local storage
let tripIndex = localStorage.getItem("TRIP_INDEX_KEY");
// using the getLocker method, retrieve the current Locker instance
let chosenTrip= trips.getTrip(tripIndex);

    
//Call this function when the page loading
summaryDisplay()