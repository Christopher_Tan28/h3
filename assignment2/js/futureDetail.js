"use strict"
// THis js is used for future and past trip


/* This function runs when click the button
and it will redirect us to viewpage */
function viewPage(index)
{
    console.log(index)
    localStorage.setItem("TRIP_INDEX_KEY",index)
    // save the specific date time
    window.location="viewpage.html"

}

// THis is sort info by the date
let futureTrip=[]
for(let i=0;i<trips.count;i++)
{
    let todayDate=new Date().getTime()
    if(trips.trips[i].date.getTime()>todayDate)
    {
        futureTrip.push(trips.trips[i])
    }
}
// THis function is to display info on the web page
function displayTripInfo(tripList)
{
    let nameDisplay=document.getElementById("column1")
    let dateDisplay=document.getElementById("column4")
    let distanceDisplay=document.getElementById("column3")
    let routeDisplay=document.getElementById("column2")
    let nameString=''
    let dateString=''
    let distanceString=''
    let routeString=''
    for(let i=0;i<tripList.length;i++)
    {
        let index=trips.trips.indexOf(tripList[i])
        nameString+=tripList[i].name+'\n\n\n\n\n\n'
        dateString+=tripList[i].date.toDateString() +"<br><br>"+`<button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored" onclick=\'viewPage(${index})\'>View</button>`+'<br><br><br>'
        distanceString+=tripList[i].lengthOfTrip +'Km'+'\n\n\n\n\n\n'
        routeString+="Country: "+tripList[i].route[0].country+'\n'+"Starting airport: "+tripList[i].startPoint+'\n'+'Destination: '+tripList[i].destination.city+"\n"+'Stops: '+tripList[i].stops+'\n\n\n'
        
    }
    nameDisplay.innerText=nameString
    dateDisplay.innerHTML=dateString
    distanceDisplay.innerText=distanceString
    routeDisplay.innerText=routeString

}
    




//call this function to show the info 
displayTripInfo(futureTrip)




